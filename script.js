console.log('loading...')
var currentVal = 0;
var newVal = 0;

element = document.getElementById('result')

function initialVal() {
    currentVal = Number(document.getElementById("result").innerText);
    newVal = 0;
}


function setValue() {
    document.getElementById('result').innerText = newVal;
}

function increment() {
    initialVal();
    newVal = currentVal + 1;
    setValue();
    changeColor();
}

function decrease() {
    initialVal();
    newVal = currentVal - 1;
    setValue();
    changeColor();
}

function reset() {
    initialVal();
    newVal = 0;
    setValue();
    changeColor();
}


function changeColor() {
    if (newVal == 0) {
        element.style.color = "blue";
    } else if (newVal > 0) {
        element.style.color = "green";
    } else {
        element.style.color = "red";
    }
}